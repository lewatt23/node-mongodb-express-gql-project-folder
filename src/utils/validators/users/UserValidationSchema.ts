import { IsEmail, IsDefined, Length, IsAlphanumeric } from "class-validator";

export class UserValidationSchema {
  @Length(5, 15)
  @IsAlphanumeric()
  @IsDefined()
  username: string;

  @Length(2, 15)
  @IsDefined()
  password: string;

  @IsEmail()
  @IsDefined()
  email: string;

  constructor(userInfo: any) {
    this.username = userInfo.username;
    this.password = userInfo.password;
    this.email = userInfo.email;
  }
}
