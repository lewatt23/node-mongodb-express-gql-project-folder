import express, {
  Response as ExResponse,
  Request as ExRequest,
  Application,
  NextFunction,
} from "express";
import cors from "cors";
import helmet from "helmet";

import bodyParser from "body-parser";
import swaggerUi from "swagger-ui-express";

import { requestLoggerMiddleware } from "../utils/request.logger.middleware";
import { RegisterRoutes } from "../routes/routes";
import morganMiddleware from "../utils/morgan";
import { ValidateError } from "tsoa";
import Logger from "../utils/winston";

const app: Application = express();

// morgan traffic logger
app.use(morganMiddleware);
app.use(helmet({ contentSecurityPolicy: false }));

//setting up cors
app.use(cors());

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(requestLoggerMiddleware);
RegisterRoutes(app);

try {
  const swaggerDocument = require("../routes/swagger.json");
  app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
} catch (err) {
  console.error("Unable to read swagger.json", err);
}

app.use(function notFoundHandler(_req, res: ExResponse) {
  res.status(404).send({
    message: "Not Found",
  });
});

//catch errors validation
app.use(function errorHandler(
  err: unknown,
  req: ExRequest,
  res: ExResponse,
  next: NextFunction
): ExResponse | void {
  if (err instanceof ValidateError) {
    console.warn(`Caught Validation Error for ${req.path}:`, err.fields);

    Logger.error(err.message);
    return res.status(422).json({
      message: "Validation Failed",
      details: err?.fields,
    });
  }
  if (err instanceof Error) {
    Logger.error(err.message);
    return res.status(500).json({
      message: "Internal Server Error",
      details: err?.message,
    });
  }

  next();
});

export { app };
