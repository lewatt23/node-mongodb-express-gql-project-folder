import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import SMTPPool from "nodemailer/lib/smtp-pool";
import dotenv from "dotenv";

// get config vars
dotenv.config();

const createMailTransport = (configOptions: SMTPPool.Options): Mail => {
  const options: SMTPPool.Options = {
    port: Number(process.env.MAILER_PORT),
    host: process.env.MAILER_HOST,
    secure: process.env.NODE_ENV === "development", // use TLS

    auth: {
      pass: process.env.MAILER_PASSWORD,
      user: process.env.MAILER_USER,
    },
    ...configOptions,
  };

  return nodemailer.createTransport(options);
};

export const mailer = createMailTransport({ pool: true });
