import { ApolloServer } from "apollo-server-express";
import { Application } from "express";
import mongoose from "mongoose";
import schema from "./modules";
import { app } from "./config/app";

import "./utils/connection";

function startApolloServer(app: Application) {
  const server = new ApolloServer({
    schema,
    playground: process.env.NODE_ENV === "development" ? true : false,
    introspection: true,
    tracing: true,
  });
  server.start();

  server.applyMiddleware({
    app,
    cors: true,
    onHealthCheck: () =>
      new Promise<void>((resolve, reject) => {
        if (mongoose.connection.readyState > 0) {
          resolve();
        } else {
          reject();
        }
      }),
  });

  new Promise((resolve: any) => app.listen({ port: 4000 }, resolve));
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
  return { server, app };
}

startApolloServer(app);
