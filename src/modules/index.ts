import { SchemaComposer } from "graphql-compose";
import { UserMutation, UserQuery } from "./auth/users/users.gql";

import "../utils/connection";

const schemaComposer = new SchemaComposer();

schemaComposer.Query.addFields({
  ...UserQuery,
});

schemaComposer.Mutation.addFields({
  ...UserMutation,
});

export default schemaComposer.buildSchema();
