import { Document, Model, Types } from "mongoose";
import { UserStatics } from "./users.statics";

/**
 * Since null is not allowed on OpenAPI, use this to represent null values on Spec
 */
export type NullValue = null;

export type InferMongooseProps<T> = {
  [K in keyof T]: T[K];
};
export interface IDocTimestamp {
  createdAt: Date;
  updatedAt: Date;
}
export interface IUserModel
  extends Model<IUser>,
    InferMongooseProps<typeof UserStatics> {}

export enum EUserRole {
  SuperAdmin = "super-admin",
  Admin = "admin",
  Editor = "editor",
  User = "user",
}

export interface IUser extends Document {
  _id: Types.ObjectId;
  uid: Types.ObjectId;
  name: string;
  email: string;
  username: string;
  role: EUserRole[];
}
