import { Schema } from "mongoose";
import { EUserRole, IUser } from "./users.types";

export const UserSchema = new Schema(
  {
    username: {
      type: String,
      trim: true,
      required: true,
    },
    name: String,
    email: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
      required: true,
    },
    role: {
      type: [String],
      default: [EUserRole.User],
    },
  },
  { timestamps: true }
);

UserSchema.virtual("uid", { toObject: true, toJson: true }).get(function (
  this: IUser
) {
  return this._id;
});
