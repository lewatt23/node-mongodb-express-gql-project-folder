import mongoose from "mongoose";

import { composeWithMongoose } from "graphql-compose-mongoose";
import { UserSchema } from "./users.schema";
import { IUser, IUserModel } from "./users.types";
import { MODULE_NAME_USERS } from "../config";
import { UserStatics } from "./users.statics";

UserSchema.statics = UserStatics;

export const UserModel = mongoose.model<IUser, IUserModel>(
  MODULE_NAME_USERS,
  UserSchema
);

export const UserTC = composeWithMongoose(UserModel);
