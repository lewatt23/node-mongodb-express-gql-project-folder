import * as express from "express";
import * as jwt from "jsonwebtoken";
import { Types } from "mongoose";
import { EUserRole } from "./users/users.types";
import dotenv from "dotenv";
import Logger from "../../utils/winston";

// get config vars
dotenv.config();

const secret = String(process.env.TOKEN_SECRET);
export type SecuredRequest = express.Request & {
  auth: IAuthAccessTokenPayload;
};

export interface IAuthAccessTokenPayload {
  uid: Types.ObjectId;
  roles: EUserRole[];
}

export const verifyToken = (
  token: string
): Promise<IAuthAccessTokenPayload> => {
  return new Promise((resolve, reject) =>
    jwt.verify(token, secret, (err, payload) => {
      if (err) {
        return reject(err);
      }

      try {
        const auth: IAuthAccessTokenPayload = {
          uid: Types.ObjectId((payload as any).uid),
          roles: (payload as any).roles,
        };
        return resolve(auth);
      } catch (e) {
        return reject("Could not parse user authorization token.");
      }
    })
  );
};

export async function expressAuthentication(
  request: express.Request,
  securityName: string,
  scopes?: string[]
): Promise<void> {
  if (securityName === "Auth") {
    const rawToken = String(request.get("authorization") || "");
    const token = rawToken.replace(/Bearer\s/, "");

    let auth: IAuthAccessTokenPayload;

    try {
      auth = await verifyToken(token);
      (request as SecuredRequest).auth = auth;
    } catch (e) {
      Logger.log({
        level: "error",
        message: "can't check auth token",
      });

      throw new Error("jwt token user cannot be verified BIG TROUBLE");
    }
  }
}
