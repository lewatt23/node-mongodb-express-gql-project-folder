export type Email = string;

export interface AuthSignUpInput {
  name: string;
  username: string;
  email: Email;
  password: string;
}

export interface AuthSignInInput {
  username: string;

  password: string;
}

export interface AuthVerifyInput {
  refreshToken: string;
}

export interface AuthMeInInput {
  accessToken: string;
}
