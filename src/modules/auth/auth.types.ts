import { EUserRole } from "./users";

export interface User {
  _id: string;
  uid: string;
  name?: string;
  email: string;
  username?: string;
  role?: EUserRole[];
}
export interface SignInUser {
  _id: string;
  uid: string;
  name?: string;
  email: string;
  username?: string;
  role?: EUserRole[];
  accessToken: string;
  refreshToken: string;
}
