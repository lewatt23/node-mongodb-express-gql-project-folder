import { SessionsModel } from "./sessions/sessions.model";
import httpStatusCodes from "http-status-codes";

import {
  Route,
  Request,
  Body,
  Get,
  Post,
  Security,
  Controller,
  SuccessResponse,
} from "tsoa";
import { UserModel, IUser } from "./users";
import express from "express";
import {
  AuthSignInInput,
  AuthSignUpInput,
  AuthVerifyInput,
  AuthMeInInput,
} from "./auth.utils";
import { PasswordsModel } from "./passwords/passwords.model";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import randtoken from "rand-token";

import { SignInUser, User } from "./auth.types";
import moment from "moment";
import { SecuredRequest } from "./authentication";
import {
  ApplicationError,
  Authentication_401,
  Conflict_409,
  Invalid_400,
  NotFound_404,
  Unprocessable_422,
} from "../../utils/errors/ApplicationError";
import { validateUserRegistration } from "../../utils/validators/users/UserValidationProcessor";

// get config vars
dotenv.config();

@Route("auth")
export class UsersController extends Controller {
  /**
   * Get all users on the application
   */
  @Get("all")
  @SuccessResponse(httpStatusCodes.OK, "Users All users")
  public async getAll(): Promise<User[]> {
    try {
      let items: any[] = await UserModel.find({});
      items = items.map((item) => {
        return {
          id: item._id,
          email: item.email,
          name: item.name,
          role: item.role,
        };
      });
      return items;
    } catch (err) {
      this.setStatus(httpStatusCodes.INTERNAL_SERVER_ERROR);
      console.error("Caught error", err);
      return [];
    }
  }

  /**
   * Create User Authorization credential
   * @param req
   * @param body
   */
  @Post("sign-up")
  @SuccessResponse(httpStatusCodes.OK, "User Authorization Credentials")
  public async signUp(
    @Request() req: express.Request,
    @Body() body: AuthSignUpInput
  ): Promise<User> {
    try {
      if (!body.username && !body.email && !body.password) {
        throw new Unprocessable_422(
          `One of the following was not provided password, email or username.`
        );
      }

      let validationErrors: any[] = await validateUserRegistration(body);

      if (validationErrors.length > 0) {
        throw new Invalid_400(
          `New user registration errors  ${JSON.stringify(
            validationErrors[0].constraints
          )}`
        );
      }

      if (
        await UserModel.checkDuplicateUsernameOrEmail("username", body.username)
      ) {
        throw new Conflict_409(
          `A User with a userName of ${body.username} was previously created!`
        );
      }

      if (await UserModel.checkDuplicateUsernameOrEmail("email", body.email)) {
        throw new Conflict_409(
          `A User with a email of ${body.email} was previously created!`
        );
      }

      const userNew = new UserModel({
        username: body.username,
        name: body.name,
        email: body.email,
      });
      const user = await userNew.save();
      if (user) {
        await PasswordsModel.create({
          uid: user._id,
          value: bcrypt.hashSync(body.password, 8),
        });
      }

      return {
        _id: user._id.toHexString(),
        name: user.name,
        uid: user._id.toHexString(),
        email: user.email,
      };
    } catch (err) {
      throw new ApplicationError(err?.message);
    }
  }

  /**
   * Create User Authorization credential
   * @param req
   * @param body
   */
  @Post("sign-in")
  @SuccessResponse(httpStatusCodes.OK, "User Authorization Credentials")
  public async signIn(
    @Request() req: express.Request,
    @Body() body: AuthSignInInput
  ): Promise<SignInUser> {
    try {
      if (!body.username && !body.password) {
        throw new Invalid_400();
      }

      const user: IUser | null = await UserModel.findOne({
        email: body.username,
      });

      if (!user) {
        throw new Invalid_400(
          `user with username ${body.username} was not found please try again`
        );
      }
      const password = await PasswordsModel.findOne({ uid: user._id });

      if (!password) {
        throw new Invalid_400(`Password or username not correct`);
      }

      const passwordIsValid = bcrypt.compareSync(
        req.body.password,
        password.value
      );

      if (!passwordIsValid) {
        throw new Invalid_400(`Password or username not correct`);
      }
      const secret = String(process.env.TOKEN_SECRET);

      //creating user access token
      const accessToken = jwt.sign({ uid: user._id, role: user.role }, secret, {
        expiresIn: "4h", // 4 hours
      });

      // creating refreshtoken
      const newSession = new SessionsModel({ uid: user._id });

      // save to get confident and verified sid
      const session = await newSession.save();

      const refreshToken = jwt.sign(
        { uid: user._id, sid: session._id },
        secret,
        {
          expiresIn: "3 days", // 3 days
        }
      );

      // adding date
      const days = 3;
      const newDate = new Date(Date.now() + days * 24 * 60 * 60 * 1000);

      //creating the session and storing it
      session.accessToken = accessToken;
      session.uid = user._id.toHexString();
      session.refreshToken = refreshToken;
      session.expired_at = newDate; //current day + 3 days

      await session.save();

      if (session) {
        return {
          _id: user._id.toHexString(),
          name: user.name,
          uid: user._id.toHexString(),
          email: user.email,
          accessToken: accessToken,
          refreshToken: refreshToken,
        };
      } else {
        throw new NotFound_404();
      }
    } catch (err) {
      this.setStatus(httpStatusCodes.INTERNAL_SERVER_ERROR);
      console.error("Caught error", err);
      return err;
    }
  }

  /**
   * Get the current user information. Details of the requester.
   * @param req
   */
  @Post("me")
  @Security("Auth")
  public async index(
    @Request() req: SecuredRequest,
    @Body() body: AuthMeInInput
  ): Promise<User> {
    const user = await UserModel.findById(req.auth.uid).exec();

    if (!user) {
      throw new NotFound_404("User not Found in systems.");
    }

    return {
      _id: user._id.toHexString(),
      name: user.name,
      uid: user._id.toHexString(),
      email: user.email,
    };
  }

  /**
   * Create User Authorization credential
   * @param req
   * @param body
   */

  @Post("refresh-token")
  @SuccessResponse(httpStatusCodes.OK, "User Authorization Credentials")
  public async verify(
    @Request() req: express.Request,
    @Body() body: AuthVerifyInput
  ): Promise<SignInUser> {
    try {
      if (!body.refreshToken) {
        throw new NotFound_404();
      }

      // disable last session
      const sessionOld = await SessionsModel.findOne(
        {
          $and: [
            {
              refreshToken: body.refreshToken,
            },
            {
              destroyed: false,
            },
          ],
        },
        {
          $set: { destroyed: true },
        }
      );

      if (!sessionOld) {
        throw new NotFound_404();
      }

      const user = await UserModel.findOne({ _id: sessionOld.uid });

      if (!user) {
        throw new NotFound_404();
      }

      const secret = String(process.env.TOKEN_SECRET);

      //creating user access token
      const accessToken = jwt.sign({ uid: user._id, role: user.role }, secret, {
        expiresIn: "4h", // 4 hours
      });

      // creating refreshtoken
      const newSession = new SessionsModel({ uid: user._id });

      // save to get confident and verified sid
      const session = await newSession.save();

      const refreshToken = jwt.sign(
        { uid: user._id, sid: session._id },
        secret,
        {
          expiresIn: "3 days", // 3 days
        }
      );

      // adding date
      const days = 3;
      const newDate = new Date(Date.now() + days * 24 * 60 * 60 * 1000);

      //creating the session and storing it
      session.accessToken = accessToken;
      session.uid = user._id.toHexString();
      session.refreshToken = refreshToken;
      session.expired_at = newDate; //current day + 3 days

      await session.save();

      if (session) {
        return {
          _id: user._id.toHexString(),
          name: user.name,
          uid: user._id.toHexString(),
          email: user.email,
          accessToken: accessToken,
          refreshToken: refreshToken,
        };
      } else {
        throw new NotFound_404();
      }
    } catch (err) {
      this.setStatus(httpStatusCodes.INTERNAL_SERVER_ERROR);
      console.error("Caught error", err);
      return err;
    }
  }
}
