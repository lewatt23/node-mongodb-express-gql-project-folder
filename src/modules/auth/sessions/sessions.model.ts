import { SessionSchema } from "./sessions.schema";
import mongoose from "mongoose";

import { composeWithMongoose } from "graphql-compose-mongoose";

import { MODULE_NAME_SESSIONS } from "../config";
import { ISessions } from "./sessions.types";

export const SessionsModel = mongoose.model<ISessions, any>(
  MODULE_NAME_SESSIONS,
  SessionSchema
);
