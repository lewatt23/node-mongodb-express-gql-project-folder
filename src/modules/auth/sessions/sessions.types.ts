import { Document, Types } from "mongoose";

export interface ISessions extends Document {
  uid: Types.ObjectId;
  accessToken: string;
  refreshToken: string;
  expired_at: Date;
  used: boolean;
  destroyed: boolean;
}
