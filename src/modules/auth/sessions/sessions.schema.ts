import { Schema } from "mongoose";

export const SessionSchema = new Schema(
  {
    uid: { type: Schema.Types.ObjectId, required: true }, // uid

    accessToken: { type: String, select: 0 },
    refreshToken: { type: String, select: 0 },
    expired_at: { type: Date },
    used: { type: Boolean, default: true },
    destroyed: { type: Boolean, default: false },
  },
  { timestamps: true }
);
