import { Schema } from "mongoose";

export const PasswordSchema = new Schema(
  {
    uid: { type: Schema.Types.ObjectId, required: true }, // uid

    value: { type: String, required: true },
  },
  { timestamps: true }
);
