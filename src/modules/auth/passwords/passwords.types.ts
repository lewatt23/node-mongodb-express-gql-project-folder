import { Document, Types } from "mongoose";

export interface IPassword extends Document {
  uid: Types.ObjectId;

  value: string;
}
