import { PasswordSchema } from "./passwords.schema";
import mongoose from "mongoose";

import { composeWithMongoose } from "graphql-compose-mongoose";

import { MODULE_NAME_PASSWORD } from "../config";
import { IPassword } from "./passwords.types";

export const PasswordsModel = mongoose.model<IPassword, any>(
  MODULE_NAME_PASSWORD,
  PasswordSchema
);
export const PasswordTC = composeWithMongoose<IPassword, any>(PasswordsModel);
